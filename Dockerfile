
# args

FROM node:18.17.1-alpine as builder
# staging || production
ARG STAGING=staging

WORKDIR /web
COPY package*.json ./
COPY yarn.lock ./

RUN yarn install --immutable
COPY . .
RUN yarn build:${STAGING}


# production environment
FROM nginx:1.21.0-alpine
# copy config files
COPY /.nginx/nginx.conf /etc/nginx/conf.d/default.conf
# copy build files
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /web/build /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]
