const fs = require('fs');
const axios = require('axios');
const { exec } = require('child_process');

async function generateModelTypes() {
  try {
    const folderSource = '../src/models';

    const generateFiles = (response, sourcePath = folderSource) => {
      if (sourcePath.includes('database')) return;
      if (!fs.existsSync(sourcePath)) {
        fs.mkdirSync(sourcePath);
      }
      Object.keys(response).forEach((key) => {
        const content = response[key];
        const fullPath = sourcePath + '/' + key;
        // file
        if (typeof content === 'string') {
          fs.writeFileSync(fullPath, content, {
            encoding: 'utf-8',
          });
        } else {
          generateFiles(content, fullPath);
        }
      });
    };
    const response = (await axios.get('http://192.168.100.7:3004/types')).data;
    // const response = (await axios.get('http://localhost:3000/types')).data;

    generateFiles(response);
    exec(`npx prettier ${folderSource} --write`, (err) => {
      if (err) {
        console.error('could not execute command: ', err);
        return;
      }
    });
  } catch (error) {
    console.error('error', error);
  }
}
// generateLanguageKeysType();
generateModelTypes();
