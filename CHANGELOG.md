# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/production-ready-demo/web-app/compare/production-0.1.1...production-0.1.2) (2024-02-15)

### [0.1.1](https://gitlab.com/production-ready-demo/web-app/compare/production-0.1.0...production-0.1.1) (2024-02-14)

## [0.1.0](https://gitlab.com/production-ready-demo/web-app/compare/production-0.0.9...production-0.1.0) (2024-02-14)


### Bug Fixes

* pem file rename ([db14a12](https://gitlab.com/production-ready-demo/web-app/commit/db14a12384c3f0c1fff4ac696002fc1a93fc82a1))

### [0.0.9](https://gitlab.com/production-ready-demo/web-app/compare/production-0.0.8...production-0.0.9) (2024-02-14)


### Features

* add keys ([8154018](https://gitlab.com/production-ready-demo/web-app/commit/81540180096ee58b7b6e81c3eac16fb91961af7e))

### [0.0.8](https://gitlab.com/production-ready-demo/web-app/compare/production-0.0.6...production-0.0.8) (2024-02-14)

### [0.0.7](https://gitlab.com/production-ready-demo/web-app/compare/staging-0.0.2...staging-0.0.7) (2024-02-14)


### Features

* add link list ([586aee5](https://gitlab.com/production-ready-demo/web-app/commit/586aee5910644a979f705310284f65027c0dc800))

### [0.0.6](https://gitlab.com/production-ready-demo/web-app/compare/production-0.0.5...production-0.0.6) (2024-02-13)

### [0.0.5](https://gitlab.com/production-ready-demo/web-app/compare/production-0.0.4...production-0.0.5) (2024-02-11)


### Features

* add link list ([586aee5](https://gitlab.com/production-ready-demo/web-app/commit/586aee5910644a979f705310284f65027c0dc800))

### 0.0.4 (2024-02-10)

### 0.0.3 (2024-02-10)

### [0.0.2](https://gitlab.com/production-ready-demo/web-app/compare/staging-0.0.1...staging-0.0.2) (2024-02-10)

### 0.0.1 (2024-02-10)
