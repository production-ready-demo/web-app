// /** @type {import('tailwindcss').Config} */
// module.exports = {
//   content: ['./src/**/*.{js,jsx,ts,tsx}', './index.html', 'node_modules/preline/dist/*.js'],
//   darkMode: 'class',
//   theme: {
//     extend: {
//       // extend base Tailwind CSS utility classes
//     },
//   },
//   // presets: [require('@acmecorp/tailwind-base')],
//   plugins: [
//     require('@tailwindcss/forms')({
//       strategy: 'class',
//     }),
//     require('preline/plugin'),
//   ],
// };
const colors = require('tailwindcss/colors');
const plugin = require('tailwindcss/plugin');
// TODO optimize
const safeList = [];
// for (const key in colors) {
//   safeList.push(`/text-${key}$/`);
//   safeList.push(`/bg-${key}$/`);
//   safeList.push(`/border-${key}$/`);
// }
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}', './node_modules/@tremor/**/*.{js,ts,jsx,tsx}', './index.html'],
  theme: {

  },
  variants: {
    textColor: ({ after }) => after(['invalid']),

    scrollbar: ['rounded'],
  },
  plugins: [
    require('@tailwindcss/forms')({
      strategy: 'class',
    }),
    require('@tailwindcss/line-clamp'),
    require('tailwind-scrollbar'),
    plugin(function ({ addVariant, e }) {
      addVariant('invalid', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`invalid${separator}${className}`)}:invalid`;
        });
      });
    }),
    require('tailwindcss-rtl'),
    require('@tailwindcss/typography'),
  ],
};
