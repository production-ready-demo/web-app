#!/bin/bash

echo "collecting stas for badges"
commits=`git rev-list --all --count`
latest_release_tag=$(git describe --tags `git rev-list --tags --max-count=1`)
echo $latest_release_tag
APP_VERSION="${latest_release_tag:1}"
echo $APP_VERSION
echo $CI_WEB_HOOKS
echo '{"commits":"'$commits'","release_tag":"'$latest_release_tag'"}' > .public/badges.json
