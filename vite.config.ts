import react from '@vitejs/plugin-react';
import { defineConfig, splitVendorChunkPlugin } from 'vite';
import checker from 'vite-plugin-checker';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3005,
  },
  plugins: [
    react(),
    tsconfigPaths(),
    splitVendorChunkPlugin(),
    // eslint({
    //   cache: true,
    //   emitWarning: false,
    //   emitError: true,
    // }),
    // TODO
    checker({
      typescript: true,
      overlay: {
        initialIsOpen: false,
      },
    }),
  ],
  build: {
    sourcemap: false,
    emptyOutDir: true,
    outDir: 'build',
    chunkSizeWarningLimit: 300,
  },
});
