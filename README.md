# @production-ready-demo/web-app

## Requirements

| Requirement | Version     |
|-------------|-------------|
| `NodeJs`    | `>=18.17.1` |
| `yarn`      | `>=1.22.19` |

## Setup a development environment

### Install dependencies by running

```bash
# make sure you have yarn is installed
# run
yarn
```

### Run locally

```bash
yarn dev
```

### Environment Variables

Create a `.env` file or `.env.${mode}` that contains the following variables

```bash

#
VITE_API_KEY=pk_xxxxxxxxxxxxx

```

Or copy `example.env` that contains all environment variables
```bash

cp example.env .env
```

## Build the app

### Create new release

1. Make sure you're in the `main branch` and run one of this two commands. A new Tag will be created.

```bash
### To  Build staging release this is for staging or testing
yarn release:staging

### To Build stable release this is production
yarn release:production
```

2. Push the tag to build the docker image in `circleci`

   ```bash
    # staging tags with prefix staging-{v}
    git push origin staging-{v}

    # stable tags with prefix stable-{v}
    git push origin stable-{v}

   ```
### Docker
#### Build an image

All images should be tagged `:staging:<app-version>`  for staging release & `:stable:<app-version>` for the stable release or `production`
#### Example
to build an image we can use `.build-docker-image.sh` script
```bash
# add exec permission
chmod +x ./build-docker-image.sh
# first argument should be production or staging
# staging for the staging version tagged with :staging:<app-version>
# --push is optional  to auto push to the gitlab registry after building the image
./build-docker-image.sh <production | staging> [--push]
```
