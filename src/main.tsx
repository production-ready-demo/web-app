import React, { Fragment } from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

import App from './App';
import './index.css';
import { isInProduction } from './utils/constant';

const container = document.getElementById('root');
const root = createRoot(container!);
/**
 * remove strict mode in favor of react-stately
 * See: https://github.com/adobe/react-spectrum/issues/3288
 *      https://github.com/adobe/react-spectrum/issues/779
 * See more: https://github.com/adobe/react-spectrum/issues?q=is%3Aissue+label%3A%22strict+mode%22+is%3Aclosed
 *
 */
const Container = isInProduction ? Fragment : React.StrictMode;
root.render(
  <Container>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Container>,
);
