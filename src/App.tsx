// providers
import { Suspense, lazy } from 'react';
import { QueryClientProvider } from 'react-query';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { queryClientManager } from './api/QueryClientManager';
import FallBackSuspense from './components/FallBackSuspense';

const Home = lazy(() => import('./pages/Home'));
function App() {
  return (
    <Suspense fallback={<FallBackSuspense />}>
      <QueryClientProvider client={queryClientManager.queryClient}>
        <BrowserRouter>
          <Switch>
            <Route exact path={'/'}>
              <Redirect to={'/links'} />
            </Route>
            <Route path={'/links'}>
              <Home />
            </Route>
          </Switch>
        </BrowserRouter>
      </QueryClientProvider>
    </Suspense>
  );
}

export default App;
