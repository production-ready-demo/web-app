import packageJson from '../../package.json';

if (window) {
  window.appVersion = packageJson.version;
  window.mode = import.meta.env.MODE;
}

export const APP_VERSION = packageJson.version;
export const isInDevelopmentMode = import.meta.env.MODE === 'development';
export const isInTestModel = import.meta.env.MODE === 'staging';
export const isInProduction = import.meta.env.MODE === 'production';

/**
 *
 */

type ConfigType = {
  API_URL: string;
};

const DEV_CONFIG: ConfigType = {
  API_URL: 'http://localhost:3000',
};
/***
 *
 */
const TEST_CONFIG: ConfigType = {
  API_URL: `${window.location.origin}/api`,
};
/**
 *
 */
const PRODUCTION_CONFIG: ConfigType = {
  API_URL: `${window.location.origin}/api`,
};
export const CONFIG = isInDevelopmentMode ? DEV_CONFIG : isInTestModel ? TEST_CONFIG : PRODUCTION_CONFIG;
//
