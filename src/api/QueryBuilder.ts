import { CONSTANTS } from '../utils';

// # test
export class QueryBuilder {
  private url: URL;
  constructor(baseUrl: string = CONSTANTS.CONFIG.API_URL) {
    let newBaseUrl = baseUrl;
    if (newBaseUrl.endsWith('/')) {
      newBaseUrl = newBaseUrl.slice(0, -1);
    }
    this.url = new URL(newBaseUrl);
  }
  /**
   *
   */
  setPage(page?: number | string) {
    if (page) this.url.searchParams.set('page', page.toString());
    return this;
  }

  /**
   *
   */
  setLimit(limit?: string | number) {
    if (limit) this.url.searchParams.set('limit', limit.toString());
    return this;
  }
  /**
   *
   */
  setQueryParam(key: string, value?: unknown) {
    if (value) {
      if (Array.isArray(value) && value.length === 0) {
        this.url.searchParams.delete(key);
      } else {
        const parsedValue = Array.isArray(value) ? value.join(',').trim() : value.toString().trim();
        this.url.searchParams.set(key, parsedValue);
      }
    } else {
      this.url.searchParams.delete(key);
    }
    return this;
  }
  /**
   *
   * @param key name of the key that will sort with
   * @param direction `1` for "asc" or `-1` for `desc`
   */
  setSort(key?: string, direction: 'desc' | 'asc' = 'asc') {
    if (!key) {
      return this;
    }
    this.url.searchParams.set('sort', key);
    this.url.searchParams.set('direction', direction);
    return this;
  }

  /**
   * The function returns the search portion of the URL.
   * @returns The search portion of the URL.
   */
  get search() {
    return this.url.search.toString();
  }

  private setPathURL(url?: string) {
    if (url) {
      const newUrl = url.startsWith('/') ? url : '/' + url;
      if (this.url.pathname === '/') this.url.pathname = newUrl;
      else this.url.pathname += newUrl;
    }
    return this;
  }
  /**
   *
   */
  build() {
    return this.url.toString();
  }
  /**
   * How to use it
   * ```typescript
   *
   * const url = QueryBuilder.generateURL('/users')
   *              .setPage(2)
   *              .build();
   * // http://API_URL/users?page=2
   *```
   */
  static generateURL(pathname?: string, baseUrl?: string | undefined) {
    return new QueryBuilder(baseUrl).setPathURL(pathname);
  }

  static generateSearchParams() {
    const queryBuilder = new QueryBuilder(window.location.origin).setPathURL(window.location.pathname);
    queryBuilder.url.search = window.location.search;
    return queryBuilder;
  }
}
