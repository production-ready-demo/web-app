import { QueryClient, QueryKey } from 'react-query';


/** */

const DEFAULT_CACHE_TIME = 300000;

/** */

export const queryKeys = {
  NOTIFICATIONS_COUNT: [`fetch-notification-count`],
} as const;

/**
 * Manage `react-query` cached data
 */

class QueryClientManager {
  private readonly _queryClient: QueryClient;
  constructor() {
    this._queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          onError: this.onError,
          refetchOnWindowFocus: false,
          cacheTime: DEFAULT_CACHE_TIME,
        },
      },
    });
  }

  //

  onError(error: unknown) {

    // monitoringService.error({
    //   title: 'Fetch Error',
    //   content: error,
    // });
  }

  /** */

  refetchQueries(keys: QueryKey) {
    this._queryClient.refetchQueries(keys);
  }

  clearCache() {
    this._queryClient.clear();
  }

  setQueryData(key: QueryKey, data: any) {
    this._queryClient.setQueryData(key, data);
  }
  resetNotificationCount() {
    this._queryClient.setQueryData<
      | {
          notifications: number;
        }
      | undefined
    >(queryKeys.NOTIFICATIONS_COUNT, (old) => {
      if (!old) {
        return old;
      }

      return { notifications: 0 };
    });
  }
  get queryClient() {
    return this._queryClient;
  }
}

/** */

export const queryClientManager = new QueryClientManager();
