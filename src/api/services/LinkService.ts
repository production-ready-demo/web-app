import axios from 'axios';

import { CONSTANTS } from '../../utils';

type LinkResponse = {
  _id: string;
  originalUrl: string;
  shortUrl: string;
  clicks: number;
  isActive: boolean;
};

const instance = axios.create({
  baseURL: CONSTANTS.CONFIG.API_URL,
});

export const linkService = {
  getLinks(params?: Record<string, string | number>) {
    return instance.get<Array<LinkResponse>>('/links', {
      params,
    });
  },
  shorten(url: string) {
    return instance.post('/links', {
      originalUrl: url,
    });
  },
  redirect(shortId: string) {
    return instance.get(`/links/${shortId}`);
  },
};
