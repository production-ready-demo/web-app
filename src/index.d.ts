declare global {
  interface Window {
    appVersion: string;
    mode: string;
    releaseDate: any;
  }
  interface Promise<T> {
    cancel: () => void;
  }
}

declare module 'xlsx-populate';
declare module 'leaflet';
declare module 'react-leaflet';

export {};
