import { useMemo } from 'react';

import { linkService } from '../api/services/LinkService';
import useFetchLinks from '../hooks/useFetchLinks';
import { CONSTANTS } from '../utils';
import { cn } from '../utils/cn';

interface HomeProps {}
const Home = ({}: HomeProps) => {
  const { refetch, data, isSuccess, isFetching } = useFetchLinks();
  async function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    // Access the form element from the event
    const form = e.currentTarget;

    // Create a new FormData object from the form
    const formData = new FormData(form);

    // Access form data using the get method of FormData
    const originalUrl = formData.get('originalUrl') as string;
    if (!originalUrl) {
      return;
    }
    try {
      await linkService.shorten(originalUrl);
      refetch();
      form.reset();
    } catch (error) {
      console.error(error);
    }
  }
  const links = isSuccess ? data.data : [];
  const { hostname, pId, serverVersion } = useMemo(() => {
    let hostname = '';
    let pId = '';
    let serverVersion = '';
    if (data?.headers) {
      hostname = data.headers['short-link-hostname'];
      pId = data.headers['short-link-pid'];
      serverVersion = data.headers['short-link-version'];
    }
    return { hostname, pId, serverVersion };
  }, [data?.headers]);
  const columns = [
    {
      dataIndex: 'originalUrl',
      title: 'Original URL',
      render: (originalUrl: string) => {
        return originalUrl;
      },
    },
    {
      dataIndex: 'shortUrl',
      title: 'Short URL',
      render: (shortUrl: string) => {
        return (
          <a target="_blank" href={CONSTANTS.CONFIG.API_URL + '/links/' + shortUrl} className="text-blue-500">
            {shortUrl}
          </a>
        );
      },
    },
    {
      dataIndex: 'clicks',
      title: 'Number of clicks',
      render: (clicks: string) => {
        return <span className="">{clicks}</span>;
      },
      colClassName: 'text-center align-middle',
    },
  ];

  return (
    <div className="p-5 mx-auto space-y-5">
      <div className="space-y-6">
        <div className="flex items-center justify-between">
          <h1 className="text-4xl font-medium">Shorten a long link</h1>
          <div>
            <span>
              Current Web App Version: <b className="">(v{CONSTANTS.APP_VERSION})</b>
            </span>
          </div>
        </div>
        <form onSubmit={onSubmit} className="space-y-4">
          <div className="flex flex-col space-y-1">
            <label htmlFor="originalUrl" className="text-lg text-gray-800">
              Paste a long URL
            </label>
            <div className="flex items-center space-x-4">
              <input
                name="originalUrl"
                id="originalUrl"
                placeholder="https://www.domain.com/long-path?param=XYZ&param2=ABC"
                className="rounded-md w-[50%] form-input"
              />

              <div className="pt-1">
                <button
                  type="submit"
                  className="text-white  bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-2.5 text-center me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Shorten your link
                </button>
              </div>
            </div>
          </div>
        </form>
        <div className="flex space-x-4">
          <button
            onClick={() => {
              refetch();
            }}
            className="text-blue-500 hover:text-blue-600"
          >
            <svg className={cn('w-5', isFetching && 'animate-spin')} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.39502 12.0014C4.39544 12.4156 4.73156 12.751 5.14577 12.7506C5.55998 12.7502 5.89544 12.4141 5.89502 11.9999L4.39502 12.0014ZM6.28902 8.1116L6.91916 8.51834L6.91952 8.51777L6.28902 8.1116ZM9.33502 5.5336L9.0396 4.84424L9.03866 4.84464L9.33502 5.5336ZM13.256 5.1336L13.4085 4.39927L13.4062 4.39878L13.256 5.1336ZM16.73 7.0506L16.1901 7.57114L16.1907 7.57175L16.73 7.0506ZM17.7142 10.2078C17.8286 10.6059 18.2441 10.8358 18.6422 10.7214C19.0403 10.607 19.2703 10.1915 19.1558 9.79342L17.7142 10.2078ZM17.7091 9.81196C17.6049 10.2129 17.8455 10.6223 18.2464 10.7265C18.6473 10.8307 19.0567 10.5901 19.1609 10.1892L17.7091 9.81196ZM19.8709 7.45725C19.9751 7.05635 19.7346 6.6469 19.3337 6.54272C18.9328 6.43853 18.5233 6.67906 18.4191 7.07996L19.8709 7.45725ZM18.2353 10.7235C18.6345 10.8338 19.0476 10.5996 19.1579 10.2004C19.2683 9.80111 19.034 9.38802 18.6348 9.2777L18.2353 10.7235ZM15.9858 8.5457C15.5865 8.43537 15.1734 8.66959 15.0631 9.06884C14.9528 9.46809 15.187 9.88119 15.5863 9.99151L15.9858 8.5457ZM19.895 11.9999C19.8946 11.5856 19.5585 11.2502 19.1443 11.2506C18.7301 11.251 18.3946 11.5871 18.395 12.0014L19.895 11.9999ZM18.001 15.8896L17.3709 15.4829L17.3705 15.4834L18.001 15.8896ZM14.955 18.4676L15.2505 19.157L15.2514 19.1566L14.955 18.4676ZM11.034 18.8676L10.8815 19.6019L10.8839 19.6024L11.034 18.8676ZM7.56002 16.9506L8.09997 16.4301L8.09938 16.4295L7.56002 16.9506ZM6.57584 13.7934C6.46141 13.3953 6.04593 13.1654 5.64784 13.2798C5.24974 13.3942 5.01978 13.8097 5.13421 14.2078L6.57584 13.7934ZM6.58091 14.1892C6.6851 13.7884 6.44457 13.3789 6.04367 13.2747C5.64277 13.1705 5.23332 13.4111 5.12914 13.812L6.58091 14.1892ZM4.41914 16.544C4.31495 16.9449 4.55548 17.3543 4.95638 17.4585C5.35727 17.5627 5.76672 17.3221 5.87091 16.9212L4.41914 16.544ZM6.05478 13.2777C5.65553 13.1674 5.24244 13.4016 5.13212 13.8008C5.02179 14.2001 5.25601 14.6132 5.65526 14.7235L6.05478 13.2777ZM8.30426 15.4555C8.70351 15.5658 9.11661 15.3316 9.22693 14.9324C9.33726 14.5331 9.10304 14.12 8.70378 14.0097L8.30426 15.4555ZM5.89502 11.9999C5.89379 10.7649 6.24943 9.55591 6.91916 8.51834L5.65889 7.70487C4.83239 8.98532 4.3935 10.4773 4.39502 12.0014L5.89502 11.9999ZM6.91952 8.51777C7.57513 7.50005 8.51931 6.70094 9.63139 6.22256L9.03866 4.84464C7.65253 5.4409 6.47568 6.43693 5.65852 7.70544L6.91952 8.51777ZM9.63045 6.22297C10.7258 5.75356 11.9383 5.62986 13.1059 5.86842L13.4062 4.39878C11.9392 4.09906 10.4158 4.25448 9.0396 4.84424L9.63045 6.22297ZM13.1035 5.86793C14.2803 6.11232 15.3559 6.7059 16.1901 7.57114L17.27 6.53006C16.2264 5.44761 14.8807 4.70502 13.4085 4.39927L13.1035 5.86793ZM16.1907 7.57175C16.9065 8.31258 17.4296 9.21772 17.7142 10.2078L19.1558 9.79342C18.8035 8.5675 18.1557 7.44675 17.2694 6.52945L16.1907 7.57175ZM19.1609 10.1892L19.8709 7.45725L18.4191 7.07996L17.7091 9.81196L19.1609 10.1892ZM18.6348 9.2777L15.9858 8.5457L15.5863 9.99151L18.2353 10.7235L18.6348 9.2777ZM18.395 12.0014C18.3963 13.2363 18.0406 14.4453 17.3709 15.4829L18.6312 16.2963C19.4577 15.0159 19.8965 13.5239 19.895 11.9999L18.395 12.0014ZM17.3705 15.4834C16.7149 16.5012 15.7707 17.3003 14.6587 17.7786L15.2514 19.1566C16.6375 18.5603 17.8144 17.5643 18.6315 16.2958L17.3705 15.4834ZM14.6596 17.7782C13.5643 18.2476 12.3517 18.3713 11.1842 18.1328L10.8839 19.6024C12.3508 19.9021 13.8743 19.7467 15.2505 19.157L14.6596 17.7782ZM11.1865 18.1333C10.0098 17.8889 8.93411 17.2953 8.09997 16.4301L7.02008 17.4711C8.06363 18.5536 9.40936 19.2962 10.8815 19.6019L11.1865 18.1333ZM8.09938 16.4295C7.38355 15.6886 6.86042 14.7835 6.57584 13.7934L5.13421 14.2078C5.48658 15.4337 6.13433 16.5545 7.02067 17.4718L8.09938 16.4295ZM5.12914 13.812L4.41914 16.544L5.87091 16.9212L6.58091 14.1892L5.12914 13.812ZM5.65526 14.7235L8.30426 15.4555L8.70378 14.0097L6.05478 13.2777L5.65526 14.7235Z"
                fill="currentColor"
              />
            </svg>
          </button>
          <div>Server Info:</div>
          <div>
            Host: <b>{hostname}</b>
          </div>
          <div>
            PID: <b>{pId}</b>
          </div>
          <div>
            Version: <b>{serverVersion}</b>
          </div>
        </div>
        <div className="relative">
          <table className="w-full text-sm text-left text-gray-500 border rounded-lg shadow-xl rtl:text-right">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50">
              <tr className="bg-gray-50">
                {columns.map((column) => (
                  <th scope="col" key={column.dataIndex} className={cn('px-6 py-3', column.colClassName)}>
                    <span>{column.title}</span>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {links.map((link) => (
                <tr key={link._id} className="border-b odd:bg-white even:bg-gray-50">
                  {columns.map((column, index) => (
                    <td scope={!index ? 'row' : undefined} key={column.dataIndex} className={cn('px-6 py-4 text-[#000]', column.colClassName)}>
                      {
                        // @ts-ignore
                        column.render(link[column.dataIndex])
                      }
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Home;
