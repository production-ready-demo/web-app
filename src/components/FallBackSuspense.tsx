import Spinner from './Spinner';

const FallBackSuspense = () => (
  <div className="flex flex-col items-center justify-center w-full h-screen space-y-2">
    <Spinner size={50} />
  </div>
);
export default FallBackSuspense;
