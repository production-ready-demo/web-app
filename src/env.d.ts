/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly MODE: 'development' | 'staging' | 'production';
  readonly VITE_STRIPE_API_KEY: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
export {};
