import React from 'react';
import { useQuery } from 'react-query';

import { linkService } from '../api/services/LinkService';

const useFetchLinks = () => {
  return useQuery(['links'], () => linkService.getLinks());
};

export default useFetchLinks;
